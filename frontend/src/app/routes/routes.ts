
import { ListPaintingsComponent } from '../layout/list-paintings/list-paintings.component';
import { AddPaintingComponent } from '../layout/add-painting/add-painting.component';
import { InfoPaintingComponent } from '../layout/info-painting/info-painting.component';

export const routes = [
    {path: 'add', component: AddPaintingComponent },
    {path: 'info/:_id', component: InfoPaintingComponent },
    {path: '', component: ListPaintingsComponent },
];