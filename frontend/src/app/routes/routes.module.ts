
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { routes } from './routes';
const appRoutes: Routes = routes;

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    declarations: [
        
    ],
    exports: [
        RouterModule
    ]
})

export class RoutesModule {}
