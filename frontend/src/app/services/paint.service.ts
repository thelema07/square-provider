
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Painting } from '../layout/add-painting/painting.data';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-HTTP-USER-ID': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Yjk5Yjg1ZTQ4NTVjZTE3ZjVhYzQ4NDciLCJhY2Nlc3MiOiJYLUhUVFAtVVNFUi1JRCIsImlhdCI6MTUzNjgwMDg2Mn0.WSnV7OgXPtlxL5h4hYhLf08KbpmwFGJ3SXQ5Vn3HaE0'
    })
};

@Injectable()
export class PaintService {

    constructor(private http:HttpClient){}

    getPaintings(){
        return this.http.get('http://localhost:7777/paintings/get_painting/');
    }

    getPaintingById(_id:string): Promise<Painting>{
        return this.http.get('http://localhost:7777/paintings/get_painting_by_id/'+_id)
            .toPromise()
            .then(response => response as Painting)
            .catch(err => this.errorHandler(err));
    }

    addPainting(paintData:Object): Promise<Painting>{
        return this.http.post('http://localhost:7777/paintings/add_painting',paintData)
            .toPromise()
            .then(response => response as Painting)
            .catch(err => this.errorHandler(err));
    }

    updatePainting(_id:string,updateData:Object): Promise<Painting>{
        return this.http.post('http://localhost:7777/paintings/update_painting/'+_id,updateData,{responseType: 'text' })
            .toPromise()
            .then(response => response as Object)
            .catch(err => this.errorHandler(err));
    }

    deletePainting(_id:string): Promise<Painting>{
        return this.http.delete('http://localhost:7777/paintings/delete_painting/'+_id,{responseType: 'text' })
            .toPromise()
            .then(response => response as Object)
            .catch(err => this.errorHandler(err));
    }

    errorHandler(error: any): Promise<any> {
        console.error('An error ocurred', error);
        return Promise.reject(error.message || error);
    }

}