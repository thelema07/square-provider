
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AddPaintingComponent } from './add-painting/add-painting.component';
import { InfoPaintingComponent } from './info-painting/info-painting.component';
import { ListPaintingsComponent } from './list-paintings/list-paintings.component';

@NgModule({
    providers: [],
    declarations: [
        AddPaintingComponent,
        InfoPaintingComponent,
        ListPaintingsComponent
    ],
    imports: [CommonModule,
        FormsModule],
    exports: [
        AddPaintingComponent,
        InfoPaintingComponent,
        ListPaintingsComponent
    ]
})

export class LayoutModule {}
