import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http/src/static_response';

import { PaintService } from '../../services/paint.service';

@Component({
  selector: 'app-info-painting',
  templateUrl: './info-painting.component.html',
  styleUrls: ['./info-painting.component.scss']
})
export class InfoPaintingComponent implements OnInit {

  public paint;
  name: string;
  author: string;
  year: number;
  style: string;
  country: string;
  description: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private paintService: PaintService
  ) { }

  ngOnInit() {
    this.getPaintById();
    console.log(this.activatedRoute.snapshot.params._id);
  }

  getPaintById(){
    console.log('Es por aqui');
    this.paintService.getPaintingById(this.activatedRoute.snapshot.params._id)
      .then(response => {
        this.paint = response;
      })
      .catch(err => {
        console.log(err);
      });
  }

  onSubmit(){
    console.log('Bang!!!');
    const updatePaint = {
      name: this.paint.name,
      author: this.paint.author,
      year: this.paint.year,
      style: this.paint.style,
      country: this.paint.country,
      description: this.paint.description
    };
    console.log(updatePaint);
    this.paintService.updatePainting(this.activatedRoute.snapshot.params._id,updatePaint)
      .then(response => {
        console.log(response);
        this.router.navigate(['']);
      })
      .catch(err => console.log(err));
  }

}
