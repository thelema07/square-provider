import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPaintingComponent } from './info-painting.component';

describe('InfoPaintingComponent', () => {
  let component: InfoPaintingComponent;
  let fixture: ComponentFixture<InfoPaintingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPaintingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPaintingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
