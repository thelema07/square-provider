import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Painting } from './painting.data';
import { PaintService } from '../../services/paint.service';

@Component({
  selector: 'app-add-painting',
  templateUrl: './add-painting.component.html',
  styleUrls: ['./add-painting.component.scss']
})
export class AddPaintingComponent implements OnInit {

  name: string;
  author: string;
  year: number;
  style: string;
  country: string;
  description: string;

  constructor(
    private paintService:PaintService,
    private router:Router
  ) { }

  ngOnInit() {
  }

  addPainting(){
    console.log('Bang!!!');
  }

  onSubmit(){
    const newPaint = {
      name: this.name,
      author: this.author,
      year: this.year,
      style: this.style,
      country: this.country,
      description: this.description
    };
    console.log(newPaint);
    this.paintService.addPainting(newPaint)
      .then(() => {
        console.log('It seems we have a new paint');
        this.router.navigate(['']);
      })
      .catch(err => console.log(err))
  }
}
