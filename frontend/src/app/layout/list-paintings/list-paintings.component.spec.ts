import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPaintingsComponent } from './list-paintings.component';

describe('ListPaintingsComponent', () => {
  let component: ListPaintingsComponent;
  let fixture: ComponentFixture<ListPaintingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPaintingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPaintingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
