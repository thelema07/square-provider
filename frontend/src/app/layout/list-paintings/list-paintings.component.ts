import { Component, OnInit } from '@angular/core';
import { PaintService } from '../../services/paint.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-paintings',
  templateUrl: './list-paintings.component.html',
  styleUrls: ['./list-paintings.component.scss']
})
export class ListPaintingsComponent implements OnInit {

  public paintings;

  constructor(
    private paintService:PaintService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getPaintings();
  }

  getPaintings(){
    this.paintService.getPaintings().subscribe(
      data => {this.paintings = data; console.log(data)},
      err => console.log(err),
      ()=> console.log('done!!!')
    );
  }

  editPaint(_id){
    console.log('Bang!!!');
    this.router.navigate(['info',_id]);
  }

  deletePaint(_id){
    console.log('aqui se borra');
    this.paintService.deletePainting(_id)
      .then(response => {
        console.log(response);
        window.location.reload();
      })
      .catch()
  }

}
