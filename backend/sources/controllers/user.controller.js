
const User = require('../model/user.model');

// Metodo para crear nuevos usuarios
exports.addUser = (req,res,next) => {

    const newUser = new User({
        email: req.body.email,
        password: req.body.password
    });    

    newUser.save()
        .then(() => {
            // res.status(200).send(result);
            return newUser.generateAuthToken();
        })
        .then(token => {
            res.header('X-HTTP-USER-ID',token).send(newUser);
        })
        .catch(err => res.status(500).send(err));
};

// Metodo middleware para autenticar todas las rutas
exports.authenticate = (req,res) => {
    let token = req.header('X-HTTP-USER-ID');
    User.findByToken(token).then((user) => {
        if(!user){
            return Promise.reject();
        }
    }).catch(() => {
        res.status(401)
        .send('Ruta de Acceso Privado - Proveer X-HTTP-USER-ID')
    });
};