
const Painting = require('../model/painting.model');
const User = require('../model/user.model');
const userAuth = require('../controllers/user.controller');

// Metodo para añadir pintura
exports.addPainting = (req,res,next) => {

    let newPainting = new Painting({
        name: req.body.name,
        author: req.body.author,
        year: req.body.year,
        style: req.body.style,
        country: req.body.country,
        description: req.body.description,
        url: req.body.url
    });

    // userAuth.authenticate(req,res);

    newPainting.save(req.body)
        .then(data => {res.status(200).send(data)})
        .catch(err => res.status(500).send(err));
};

// Metodo para consultar una o todas las pintras
exports.getPainting = (req,res,next) => {
    let query = (req.params.name) ? {name:req.params.name} : {} ;
    
    // userAuth.authenticate(req,res);

    Painting.find(query,{__v:0,url:0})
            .then(result => {
                res.status(200).send(result);
                console.log(result);
            })
            .catch(err => res.status(500).send(err));
};

exports.getPaintingById = (req,res,next) => {
    let query = req.params._id;
    Painting.findById({_id:query},{__V:0,url:0})
        .then(result => res.status(200).send(result))
        .catch(err => res.status(500).send(err));
};

// Metodo para filtar una consulta con base en lso parametros de los campos
exports.getPaintingAdv = (req,res,next) => {
    // userAuth.authenticate(req,res);    
    Painting.find(req.query.filters,{__v:0})
            .then(result => res.status(200).send(result))
            .catch(err => res.status(500).send(err));
};

// Metodo para modificar campos de la consulta
exports.updatePainting = (req,res) => {
    // userAuth.authenticate(req,res);
    console.log(req.params.id);
    console.log(req.body);
    Painting.updateOne({_id: req.params.id},{$set: req.body})
            .then(result => {
                console.log(result);
                res.status(200).send('Registro Actualizado')
            })
            .catch(err => res.status(200).send('Error en la actualizacion del registro'));
};

// Metodo para Borrar una pintura
exports.removePainting = (req,res,next) => {
    // userAuth.authenticate(req,res);
    Painting.deleteOne({_id: req.params.id})
            .then(result => res.status(200).send('Registro borrado'))
            .catch(error => res.status(500).send('Error en el Borrado del Registro'));
};
