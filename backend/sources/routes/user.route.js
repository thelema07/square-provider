
const express = require('express');

const { addUser } = require('../controllers/user.controller');

const router = express.Router();

// Archivo de rutas para usuarios
router.post('/add_user', addUser);

module.exports = router;
