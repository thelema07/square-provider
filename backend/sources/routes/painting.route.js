
const express = require('express');

const { addPainting, getPainting, getPaintingAdv,
    updatePainting, removePainting, getPaintingById } = require('../controllers/painting.controller');

const { authenticate } = require('../controllers/user.controller');

const router = express.Router();

router.post('/add_painting',addPainting);
router.get('/get_painting/:name?',getPainting);
router.get('/get_painting_by_id/:_id',getPaintingById)
router.get(`/get_painting_adv`,getPaintingAdv);
router.post('/update_painting/:id',updatePainting);
router.delete('/delete_painting/:id',removePainting);

module.exports = router;