
const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

// Esquema del Usuario
const UserSchema = new mongoose.Schema({
    email:{
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password:{
        type: String,
        required: true,
        minlength: 7
    },
    tokens:[{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

// Metodos de instancia y estaticos alojados en el esquema de mongoose
UserSchema.methods.toJSON = function(){
    let user = this;
    let userObject = user.toObject();

    return _.pick(userObject,['_id','email']);
}

UserSchema.methods.generateAuthToken = function(){
    let user = this;
    let access = 'X-HTTP-USER-ID';
    let token = jwt.sign({_id: user._id.toHexString(),access},'thelema777').toString();

    user.tokens = user.tokens.concat([{access,token}]);

    return user.save().then(() => {
        return token;
    })
}

UserSchema.statics.findByToken = function(token){
    let User = this;
    let decoded;

    try{
        decoded = jwt.verify(token,'thelema777')
    } catch (error){
        return Promise.reject();
    }

    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'X-HTTP-USER-ID'        
    });
}

module.exports = mongoose.model('User',UserSchema);