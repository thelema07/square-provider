
const mongoose = require('mongoose');

// Esquema de los cuadros
const PaintingSchema = new mongoose.Schema({
    name: {type: String, required: true},
    author: {type: String, required: true},
    year: {type: Number, required: true},
    style: {type: String, required: true},
    country: {type: String, required: true},
    description: {type: String, required: true},
    url: {type: String}
});

module.exports = mongoose.model('Painting',PaintingSchema);