// Main modules
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();

app.use('/', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,X-HTTP-USER-ID");
    res.header('Access-Control-Allow-Credentials', true);
    next();
})

// Parser de URLs
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
// Variable de Puerto 
const port = process.env.PORT || 7777;

// API REST Routes
const paintings = require('../sources/routes/painting.route');
const user = require('../sources/routes/user.route')

// Database connection
mongoose.connect('mongodb://localhost:27017/paintingsbase');
mongoose.connection.on('error',() => {
    console.log('Could not connect to database');
    process.exit();
});
mongoose.connection.once('open',() => {
    console.log('Succesfully connected to database');
});

// Definicion de Rutas
app.use('/paintings',paintings);
app.use('/user',user);

app.get('/',(req,res) => {
    res.write('Bang!!');
    res.end();
});

app.listen(port,() => {
    console.log(`Server Running o port: ${port}`);
});