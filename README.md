
// Proyecto de prueba para Extendeal =>

// Adminsitrdor de cuadros =>

// Todas las rutas estan protegidas por autenticacion, lo que bloquea el acesso si no se provee el correspondiente X-HTTP-USER-ID en los headers d la consulta =>

X-HTTP-USER-ID => 

 ------- 
 
 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Yjk5Yjg1ZTQ4NTVjZTE3ZjVhYzQ4NDciLCJhY2Nlc3MiOiJYLUhUVFAtVVNFUi1JRCIsImlhdCI6MTUzNjgwMDg2Mn0.WSnV7OgXPtlxL5h4hYhLf08KbpmwFGJ3SXQ5Vn3HaE0

 -------

 al añadirse a cualquier query se accede al microservicios =>

// La siguiente es una query para hacer consultas filtrando por cualquiera de los campos de cuadros: 

localhost:7777/paintings/get_painting/:id? - GET

// La siguiente es una query para hacer consultas que devuelve todos los cuadros si no se solicita alguno en particular, o el cuadro solicitado 

localhost:7777/paintings/get_painting_adv/?filters[year]=1941 - GET

// La siguiente query permite borrar un regitro proveyendo el id de mongo correspondiente:

localhost:7777/paintings/delete_painting/5b99c2b3c636c31cdbc674dc - DELETE

// Esta query permite añadir nuevos cuadros: 

localhost:7777/paintings/add_painting/ - POST

// Finalmente esta query actualiza cualquier campo dentro del documento:

localhost:7777/paintings/update_painting/5b970d87a249c317ac4f9672

por params se pasa el id y por post un JSON con lso datos a modificar

NOTAS GENERALES => 

Para la creacion del servicio, se deben tener en cuenta las siguitentes consideraciones

Tecnologias: NodeJS, ExpressJS, lodash(paquete de utilidades), jsonwebtoken(autenticaion),
validatorjs(validador de datos), Mongoose(Administrador de objetos para MongoDB), MongoDB(Base de Datos)

Atencion: el script de package.json npm start esta ligado a nodemon, de no estar instalado, el comando inicializador es "node server/index.js"

en la carpeta db, hay dos archivos JSON para popular la base de datos